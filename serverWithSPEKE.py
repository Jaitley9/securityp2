#Created on Wed Nov 12 17:35:10 2014

import random
import socket
import pickle

from Crypto.Cipher import AES
from Crypto.Hash import HMAC
from Crypto.Hash import SHA256

def padKey(key):
    #pad key to 16, 24, 32 bytes long
    BLOCK_SIZE = 16
    PADDING = '{'
    if not len(key) in (16, 24, 32):
        return key + (BLOCK_SIZE - len(key) % BLOCK_SIZE) * PADDING
    return key

def hashG(password):
    bytesEnc = str(password).encode(encoding='utf-8')
    h = SHA256.new()
    h.update(bytesEnc)
    return h.hexdigest()

def make_hmac(ciphertext, key):
    bytesKey = str(key).encode(encoding='utf-8')
    h = HMAC.new(bytesKey)
    h.update(ciphertext)
    return h.hexdigest()

def decrypt(ciphertext, key, iv):

    secretKey = padKey(str(key))
    cipher = AES.new(secretKey, AES.MODE_CFB, iv)
    plaintext = cipher.decrypt(ciphertext)
    return plaintext

#Creating Diffie Hellman parameters
#See notes 9-12
p = 22801763489
print('p',p)
g = 1
while(pow(g,2,p) == 1):
    g = random.randint(1, p)

print('g:',g)
b = random.randint(1,p) 
# G = H(password)^2 
hashedG = pow(int(hashG(g),16),2) 
# New, more secure, b passed to client
bkey = pow(hashedG, b, p)
print('b',b)

#Create server socket on local host
#See notes 3 and 5
serverS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverS.bind(('localhost', 51536))

#Intializing session
#See note 3
serverS.listen(1) #allows for up to x connections at a time 
clientS, clientAddr = serverS.accept()
print("Connected to: ", clientAddr)

#Sending over Diffie Hellman parameters
#See note 13
clientS.sendall(bytes(str(p), 'utf-8'))
clientS.recv(1024) #Receiving confirmation
clientS.sendall(bytes(str(g), 'utf-8'))
clientS.recv(1024) #Receiving confirmation
clientS.sendall(bytes(str(bkey), 'utf-8'))
clientS.recv(1024) #Receiving confirmation

akey = int(clientS.recv(1024))
print('a',akey)
secretKey = pow(akey, b, p)
print(secretKey)

#Chatting
while(True):
    data = clientS.recv(1024) #Receve a 1024 bytes of bytewise data
    
    loaded_data = pickle.loads(data)
    if(loaded_data == "Done!"):
        break

    #verify hmac
    hmac = make_hmac(loaded_data["ciphertext"], secretKey)
    #print("recived hmac: ", loaded_data["hmac"])
    #print("my hmac: ", hmac)
    if hmac != loaded_data["hmac"]:
        print("HMAC doesn't match. Terminating connection")
        break
    #decrypted_message
    plaintext = decrypt(loaded_data["ciphertext"], secretKey, loaded_data["iv"])
    print(plaintext.decode('utf-8'))
    clientS.sendall(bytes("Message Received", 'utf-8'))


clientS.close()
serverS.close()
