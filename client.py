#Created on Wed Nov 12 18:42:49 2014

import socket
import random

#import Crypto
from Crypto.Cipher import AES
from Crypto.Hash import HMAC
from Crypto import Random

import pickle

def padKey(key):
    #pad key to 16, 24, 32 bytes long
    BLOCK_SIZE = 16
    PADDING = '{'
    if not len(key) in (16, 24, 32):
        return key + (BLOCK_SIZE - len(key) % BLOCK_SIZE) * PADDING
    return key

def encrypt(message, key):
    #using CFB mode since other modes requires the message len 
    #to be multiples of 16 blocks

    iv = Random.new().read(AES.block_size)
    secretKey = padKey(str(key))    
    cipher = AES.new(secretKey, AES.MODE_CFB, iv)
    ciphertext = cipher.encrypt(message)
    return (ciphertext, iv)
	#padding secret key should be one time thing especially if you're performing the same padding everytime
	#Need to have a new IV every time

def make_hmac(ciphertext, key):
    #using HMAC-MD5
    bytesKey = str(key).encode(encoding='UTF-8')
    h = HMAC.new(bytesKey)
    h.update(ciphertext)
    return h.hexdigest()

clientS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientS.connect(('localhost', 51539))

#Start receiving g, p and g**b from server. Need to send confirmation
p = int(clientS.recv(1024))
clientS.sendall(bytes('Received!', 'utf-8'))
g = int(clientS.recv(1024))
clientS.sendall(bytes('Received!', 'utf-8'))
bkey = int(clientS.recv(1024))
clientS.sendall(bytes('Received!', 'utf-8'))

#Picking a and sending over g**a
a = random.randint(1, p)
aKey = pow(g, a, p)
clientS.sendall(bytes(str(aKey), 'utf-8'))

secretKey = pow(bkey, a, p)
print(secretKey)

#Chatting
print("Let's chat :)")
while(True):
    done = input("Continue? y/n \n")
    if(done == "n"):
        break
    message = input("Say something: ")
    #encrypt and hmac here
    ciphertext, iv = encrypt(message, secretKey)
    hmac = make_hmac(ciphertext, secretKey) #using same key for both right now
    #sending ciphertext, iv and hmac as dict
    data = {'ciphertext': ciphertext, 'iv': iv, 'hmac': hmac}
    data_string = pickle.dumps(data, -1)
    clientS.sendall(data_string)
    response = clientS.recv(1024)
    print(response.decode('utf-8'))

#clientS.sendall(bytes("Done!", 'utf-8'))
clientS.sendall(pickle.dumps('Done!', -1))
clientS.close()
