## To run the files:##

Use python3 to initiate the server.py

In another terminal, run the MiM.py file 

In yet another terimal, run the client.py file. 

You will interface with the terminal running the client.py file

##Description##

This all emulates a broken chat program which simply connects to and sends messages directly to the "server." There is a Diffie Hellman key exchange occuring at the iniation of the connection. The messages are being encrypted using pycrpto's AES using CFB mode with random IVs for each message. This allows us to easily encrypt the messages without padding into 16 byte blocks. We also used the hash based message authentication code scheme from pycrypto. 

The client is actually connecting directly to the man in the middle (receiving its key parameters), and his messages are being transformed before being sent to the real server.

We combat this problem using Simple Password Exponential Key Exchange (SPEKE):
- instead of directly sending g^a(modp) and g^b(modp), it sends G^a(modp) and G^b(modp)
  (where G = H(g)^2, square of hashed value of g)
- Both parties use G to calculate the new key K = (G^a(modp))^b = (G^b(modp)^a)
SPEKE prevents a Man in the Middle attack because although an attacker may have access to
the ciphertext being exhanged between the 2 parties, the attacker cannot determine the key
and, therefore, can't determine the content of the information being sent.

Files clientWithSPEKE.py and serverWithSPEKE.py work just like the original files except that
instead of performing the DH-Key exchange, these files pass the DH-with-SPEKE Key which would
prevent any potential Man in the Middle from being able to decrypt ciphertext. 
