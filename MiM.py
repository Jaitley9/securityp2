#Created on Mon 15 Dec 14:38:10 2014

#important note! When sending to a client as a server, you use the client's socket. When sending to a server as a client, you use your own socket.

#Here we're making two distinct sockets. One that is a client connecting to the server (middleS)
#The second is middleC which is a server connecting to the client
#middleS receives p and g from  generates its own parameters

import random
import socket
import pickle

from Crypto.Cipher import AES
from Crypto.Hash import HMAC
from Crypto import Random

def padKey(key):
    #pad key to 16, 24, 32 bytes long
    BLOCK_SIZE = 16
    PADDING = '{'
    if not len(key) in (16, 24, 32):
        return key + (BLOCK_SIZE - len(key) % BLOCK_SIZE) * PADDING
    return key


def make_hmac(ciphertext, key):
    bytesKey = str(key).encode(encoding='utf-8')
    h = HMAC.new(bytesKey)
    h.update(ciphertext)
    return h.hexdigest()

def encrypt(message, key):
    #using CFB mode since other modes requires the message len 
    #to be multiples of 16 blocks

    iv = Random.new().read(AES.block_size)
    secretKey = padKey(str(key))    
    cipher = AES.new(secretKey, AES.MODE_CFB, iv)
    ciphertext = cipher.encrypt(message)
    return (ciphertext, iv)
	#padding secret key should be one time thing especially if you're performing the same padding everytime
	#Need to have a new IV every time

def decrypt(ciphertext, key, iv):

    secretKey = padKey(str(key))
    cipher = AES.new(secretKey, AES.MODE_CFB, iv)
    plaintext = cipher.decrypt(ciphertext)
    return plaintext

#Creating the socket for the middle guy connected to the server
middleS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#Connecting to the server
middleS.connect(('localhost', 51538))

#Start receiving g, p and g**b from server. Need to send confirmation
p = int(middleS.recv(1024))
middleS.sendall(bytes('Received!', 'utf-8'))
g = int(middleS.recv(1024))
middleS.sendall(bytes('Received!', 'utf-8'))
bkey = int(middleS.recv(1024))
middleS.sendall(bytes('Received!', 'utf-8'))

aP = random.randint(1,p)
akeyP = pow(g, aP, p)

middleS.sendall(bytes(str(akeyP), 'utf-8'))

serverKey=pow(bkey, aP, p)

#moving on to the client

middleC = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
middleC.bind(('localhost', 51539))
middleC.listen(1) #allows for up to x connections at a time 
clientS, clientAddr = middleC.accept()

bP = random.randint(1,p)
bkeyP = pow(g, bP, p)

#Sending over Diffie Hellman parameters
clientS.sendall(bytes(str(p), 'utf-8'))
clientS.recv(1024) #Receiving confirmation
clientS.sendall(bytes(str(g), 'utf-8'))
clientS.recv(1024) #Receiving confirmation
clientS.sendall(bytes(str(bkeyP), 'utf-8'))
clientS.recv(1024) #Receiving confirmation

akey = int(clientS.recv(1024))

clientKey = pow(akey, bP, p)


#Chatting
while(True):
    data = clientS.recv(1024) #Receive a 1024 bytes of bytewise data
    
    loaded_data = pickle.loads(data)
    if(loaded_data == "Done!"):
        break

    #verify hmac
    hmac = make_hmac(loaded_data["ciphertext"], clientKey)
    #print("recived hmac: ", loaded_data["hmac"])
    #print("my hmac: ", hmac)
    if hmac != loaded_data["hmac"]:
        print("HMAC doesn't match. Terminating connection")
        break
    #decrypted_message
    plaintext = decrypt(loaded_data["ciphertext"], clientKey, loaded_data["iv"])
    print(plaintext.decode('utf-8'))
    clientS.sendall(bytes("You have been intercepted by Middle Man! (Evil laugh)", 'utf-8'))

    #send something to the server
    print("Let's have fun with these two")
    while(True):
    	done = input("Continue? y/n \n")
    	if(done == "n"):
        	break
    	message = input("Say something to send to server (preferable evil and sarcastic): ")
    	ciphertext, iv = encrypt(message, serverKey)
    	hmac = make_hmac(ciphertext, serverKey)
    	#sending ciphertext, iv and hmac as dict
    	#data = {'ciphertext': ciphertext, 'iv': iv, 'hmac': hmac}
    	data_string = pickle.dumps(data, -1)
    	middleS.sendall(data_string)
    	response = middleS.recv(1024)
    	print(response.decode('utf-8'))
    

#clientS.sendall(bytes("Done!", 'utf-8'))
clientS.sendall(pickle.dumps('Done!', -1))

clientS.close()
middleS.close()
